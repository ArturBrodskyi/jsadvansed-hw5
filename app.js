
class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
    }

    render() {
        const cardElement = document.createElement('div');
        cardElement.classList.add('card');
        cardElement.innerHTML = `
            <h3>${this.post.title}</h3>
            <p>${this.post.body}</p>
            <p><strong>${this.user.name}</strong> (${this.user.email})</p>
            <span class="delete-button">X</span>
        `;

        const deleteButton = cardElement.querySelector('.delete-button');
        deleteButton.addEventListener('click', async () => {
            await this.deletePost(this.post.id);
            cardElement.remove();
        });

        return cardElement;
    }

    async deletePost(postId) {
        try {
            const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                method: 'DELETE'
            });

            if (!response.ok) {
                throw new Error('Failed to delete post');
            }
        } catch (error) {
            console.error('There has been a problem with your delete operation:', error);
        }
    }
}

async function fetchData() {
    try {
        const userResponse = await fetch('https://ajax.test-danit.com/api/json/users');
        const postResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
        
        const users = await userResponse.json();
        const posts = await postResponse.json();
        
        return { users, posts };
    } catch (error) {
        console.error('There has been a problem with your fetch operation:', error);
    }
}

async function renderPosts() {
    const { users, posts } = await fetchData();
    const postsContainer = document.getElementById('posts-container');

    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        const card = new Card(post, user);
        postsContainer.appendChild(card.render());
    });
}

renderPosts();